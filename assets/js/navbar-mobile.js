/**
 * Mobile navigation bar scripts
 * 
 * @package acrux
 */

/* Show/hide mobile menu ---------------------------------------------------------------------------- */
$(document).ready(function(e){
  
  $(".navbar-mobile").click(function(){
    $(".navbar-mobile-content").slideDown("fast");
	  $(".navbar-mobile").slideUp("50");
    $('body').css('overflow-y','hidden');
    $( ".navbar-mobile-container" ).addClass( "navbar-mobile-open" );
  });

	$(".navbar-mobile-close").click(function(){
    $(".navbar-mobile-content").slideUp("fast");
	  $(".navbar-mobile").slideDown("50");
    $('body').css('overflow-y','scroll');
    $( ".navbar-mobile-container" ).removeClass( "navbar-mobile-open" );
    $('.expand-sub-menu').next().slideUp(300);
  });

});

$(".navbar-mobile-inner").click(function(event) {
  event.stopPropagation();
});

$(document).on('click', function(e){
    $(".navbar-mobile-content").slideUp("fast");
    $(".navbar-mobile").slideDown("50");
    $('body').css('overflow-y','scroll');
    $( ".navbar-mobile-container" ).removeClass( "navbar-mobile-open" );
    $('.expand-sub-menu').next().slideUp(300);

  });

/* Toggle mobile submenu ---------------------------------------------------------------------------- */
$(function () {
	var children = $('.navbar-mobile-container li a').filter(function () { return $(this).nextAll().length > 0 })
	$('<span class="expand-sub-menu toggle-down" onclick="toggle()"><i class="fas fa-plus"></i></span>').insertAfter(children)
	$('.navbar-mobile-container .expand-sub-menu').click(function (e) {
		$(this).next().slideToggle(300);
	return false;
	});
})

/* Rotate toggle button  */
var button_rotated = 'toggle-up';
var button_default = 'toggle-down';

function toggle() {
  var exp_sub_button = document.querySelector('.expand-sub-menu');
  if (~exp_sub_button.className.indexOf(button_default)) {
    exp_sub_button.className = exp_sub_button.className.replace(button_default, button_rotated);
  } else {
    exp_sub_button.className = exp_sub_button.className.replace(button_rotated, button_default);
  }
}