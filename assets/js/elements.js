/**
 * Misc script elements
 * 
 * @package acrux
 */

$(document).ready(function() {
	jQuery('.grid').masonry({
	  // options
	  itemSelector: '.grid-item',
	  horizontalOrder: true
	});
});
