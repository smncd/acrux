/**
 * Desktop navigation bar scripts
 * 
 * @package acrux
 */

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
      $('.navbar-desktop').css('margin','0');
      $('.navbar-desktop-logo').css('height','60px');
	    $('.navbar-desktop-logo').css('margin-top','5px');
  } else {
      $('.navbar-desktop').css('margin','20px 0');
      $('.navbar-desktop-logo').css('height','90px');
      $('.navbar-desktop-logo').css('margin-top','-10px');
  }
}

$(".menu-item-has-children").hover(function() {
    $(this).toggleClass("open-sub-menu");
  });