<?php
/**
 * Template Name: No Margin (Beta)
 *
 * Used for pages that require no margin on top or bottom
 *
 * @package acrux
 */

 // Add custom styles
 wp_register_style( 'acrux-style', get_template_directory_uri() . '/assets/css/page-no-margin.css' );
 wp_enqueue_style('acrux-style');

 // Import page layout
 require('page.php');