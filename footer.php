<?php
/**
 * Main Footer
 * 
 * @package acrux
 */
get_template_part('template-parts/footer/footer');
if (get_theme_mod('credit_footer')):?>
<div class="end-credits">
<?php $theme = wp_get_theme(get_template()); echo $theme->title?> version <?php echo $theme->version; ?> by <a href="https://twentyonepenguins.xyz">twentyonepenguins</a>
</div>
<?php endif;
if( get_theme_mod( 'cd_navbar_position', 'bottom' ) == 'bottom' ) : ?>
<div class="navbar-spacer"></div>
<?php endif;
wp_footer();?>
<div class="overlay-menu"></div>
</body>
</html>