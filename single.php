<?php 
/**
 * Post template
 *
 * @package acrux
 */
get_header(); 
?> 
<main id="primary" class="site-main<?php if (get_field('hide_top_banner_title') || has_post_thumbnail()):?>page-keyvisual<?php endif; ?>"> 
<?php
get_template_part( 'template-parts/content/entry_title_banner' ); 
get_template_part( 'template-parts/content/entry_content' ); 
?>
</main>
<?php
get_footer(); ?>