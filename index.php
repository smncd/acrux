<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @package acrux
 */
get_header(); ?>

<main id="primary" class="site-main"> 
<div class="posts-page entry-title">
<h1><?php single_post_title(); ?></h1>
	<p><strong><?php _e('Categories:') ?></strong></p>
<ul class="categories">
<ul class="categories">
<li class="cat-item"><a aria-current="page" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?php _e('All') ?></a></li>
</ul>
<?php wp_list_categories('title_li='); ?>
</ul>
</div>
<div class="clear"></div>
<div class="page-content posts-page">
	<?php if ( have_posts() ) : ?>
		
		<div class="row grid" style="padding-bottom: 50px;">
		
            <?php while ( have_posts() ) : the_post(); ?>
            
				<div class="col col-12 col-sm-6 col-md-4 grid-item <?php foreach( (get_the_category() ) as $category) : ?> category-<?php echo $category->slug . ' '; endforeach; ?>" style="margin-bottom: 30px;">
					<div class="grid-item-inner">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
						<div class="grid-item-content">
							<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
							
							
							<a class="button  has-secondary-background-color" href="<?php echo get_the_permalink(); ?>">Read more</a>
						</div>
					</div>
				</div>
				
			<?php endwhile; ?>
			
		</div>
			
		<?php 
			the_posts_pagination( array(
			    'mid_size' => 2,
			    'prev_text' => __( '<i class="fal fa-long-arrow-left"></i>', 'textdomain' ),
			    'next_text' => __( '<i class="fal fa-long-arrow-right"></i>', 'textdomain' ),
			) ); 
		?>
				
	<?php endif; ?>
</div>
</main>
<?php get_footer(); ?>