<?php
/**
 * Template part for displaying a page/posts title
 *
 * @package acrux
 */ ?>
 <?php if (!has_post_thumbnail()): ?>
<div class="entry-title">
 <?php
the_title( '<h1>', '</h1>' );
get_template_part( 'template-parts/front-end/content/entry_meta' );
?>
</div>
<?php endif; ?>