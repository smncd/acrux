<?php 
/**
 * Page content
 *
 * @package acrux
 */ ?>

<div class="page-content">
    <?php while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
    </div>
<?php endwhile; ?>
</div>