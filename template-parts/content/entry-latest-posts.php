<?php
/**
 * Template part for displaying the three latest posts on the home page
 *
 * @package acrux
 **/
	$post_page_title = get_the_title( get_option('page_for_posts', true) );
?>
	<div class="container page-content latest-posts">

	<div class="col-12 text-center latest-posts-title">
        <h2><?php echo $post_page_title ?></h2>
	</div>
		
	<div class="posts-page">
	
	<div class="row" style="margin:0 auto">
		
		<div class="col-12">
	<div class="row grid" style="padding-bottom: 0px;">
		
		<?php
				$args = array(
				'post_status' => 'publish',
				'posts_per_page' => get_theme_mod('posts_front_page_number'),
				'order' => 'DESC',
				'suppress_filters' => false
			);
			$the_query = new WP_Query($args);
			
			if($the_query->have_posts()) : ?>

			<?php while($the_query->have_posts()) : $the_query->the_post() ?>
	
            
				<div class="col col-12 col-sm-6 col-md-4 grid-item <?php foreach( (get_the_category() ) as $category) : ?> category-<?php echo $category->slug . ' '; endforeach; ?>" style="margin-bottom: 30px;">
					<div class="grid-item-inner">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
						<div class="grid-item-content">
							<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
							
							<?php the_excerpt(); ?>
							<a class="button has-secondary-background-color" href="<?php echo get_the_permalink(); ?>">Read more</a>
						</div>
					</div>
				</div>
				
			<?php endwhile; ?>
			
		</div>

				
	<?php endif; ?>



<div class="row" style="margin:0; margin-top:15px">

			<div class="col-12 text-center">
				<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="button has-third-background-color">All news</a>
			</div>

		</div>
	
	</div>
</div>
</div>
</div>