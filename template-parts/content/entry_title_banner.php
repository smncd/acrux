<?php
/**
 * Template part for displaying a page/posts thumbnail
 *
 * @package acrux
 */ 

 $image_url = get_the_post_thumbnail_url(get_the_ID(), 'page-img'); 
 if (has_post_thumbnail()): ?>
 <div class="banner">
    <div class="banner-img banner-img-small banner-overlay d-flex" style="background:url('<?php echo $image_url; ?>');">
        <div class="page-title d-flex <?php if( get_theme_mod( 'cd_navbar_position', 'top' ) == 'top' ) : ?>banner-navbar-mobile-top<?php else: ?>banner-navbar-mobile-bottom<?php endif ?>">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="container">
    <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); } ?>
    </div>
    </div>
<?php endif; ?>



<?php if (!has_post_thumbnail()): ?>
    <div class="container">
    <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); } ?>
</div>
    <div class="entry-title">
        <?php
        the_title( '<h1>', '</h1>' );
        get_template_part( 'template-parts/front-end/content/entry_meta' );
        ?>
    </div>
<?php endif; ?>