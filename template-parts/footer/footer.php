<?php 
/**
 * Footer content
 * 
 * @package acrux
 */
?>
<div class="footer">
    <a href="<?php echo home_url(); ?>">
	<?php if ( get_theme_mod( 'your_theme_logo' ) ) : ?>
 		<img src="<?php echo get_theme_mod( 'your_theme_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" >
 	<?php else : ?>
 		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acrux-logo.png"> 
	<?php endif; ?>
	</a>
    <h4><?php $site_name = get_bloginfo( 'name' ); echo $site_name; ?></h4>
    <?php if (get_option('email')):?><a class="contact" href="mailto:<?php echo get_option('email'); ?>"><i class="fas fa-envelope" style="vertical-align: middle;"></i> <?php echo get_option('email'); ?></a><?php endif ?>
      <?php wp_nav_menu(array('theme_location' => 'footer-menu', 'container_class' => 'footer-menu footer-nav-menu')); ?>
      <div class="footer-menu social-menu">
      <ul>
      <?php if (get_option('facebook')):?><li><a href="<?php echo get_option('facebook'); ?>"><i class="fab fa-facebook"></i></a></li><?php endif ?>
      <?php if (get_option('twitter')):?><li><a href="<?php echo get_option('twitter'); ?>"><i class="fab fa-twitter"></i></a></li><?php endif ?>
      <?php if (get_option('instagram')):?><li><a href="<?php echo get_option('instagram'); ?>"><i class="fab fa-instagram"></i></a></li><?php endif ?>
      <?php if (get_option('youtube')):?><li><a href="<?php echo get_option('youtube'); ?>"><i class="fab fa-youtube"></i></a></li><?php endif ?>
      <?php if (get_option('vimeo')):?><li><a href="<?php echo get_option('vimeo'); ?>"><i class="fab fa-vimeo-v"></i></a></li><?php endif ?>
      <?php if (get_option('linkedin')):?><li><a href="<?php echo get_option('linkedin'); ?>"><i class="fab fa-linkedin"></i></a></li><?php endif ?>
    </ul>
    </div>
    <?php if (get_theme_mod('searchbar_footer')): get_template_part('template-parts/front-end/searchbar/searchbar'); endif ?>

	<a href="https://twentyonepenguins.xyz" style="float:left; font-size: 1px;color: var(--color-dark) !important;">ACRUX theme by twentyonepenguins</a>
</div>

