<?php 
/**
 * Mobile header content
 * 
 * @package acrux
 */

if( get_theme_mod( 'cd_navbar_position', 'top' ) == 'top' ) : ?>
  <div class="navbar-spacer"></div>
<?php endif ?>

<div class="navbar-mobile-container <?php if( get_theme_mod( 'cd_navbar_position', 'top' ) == 'top' ) : ?>navbar-mobile-top<?php else: ?>navbar-mobile-bottom<?php endif ?>">
<div class="navbar-mobile-inner <?php if( get_theme_mod( 'cd_navbar_position', 'top' ) == 'top' ) : ?>navbar-mobile-top<?php else: ?>navbar-mobile-bottom<?php endif ?>">
<?php if( get_theme_mod( 'cd_navbar_position', 'top' ) == 'top' ) : ?>
<div class="navbar-mobile">Menu</div>
<div class="navbar-mobile-content navbar-mobile-top">
<div class="navbar-mobile-close">Close</div>
<?php else: ?>	
<div class="navbar-mobile-content navbar-mobile-bottom">
<?php endif ?>	
<div class="navbar-mobile-padding">
<a href="<?php echo home_url(); ?>">
<?php if ( get_theme_mod( 'your_theme_logo' ) ) : ?>
 				<img class="navbar-mobile-logo" src="<?php echo get_theme_mod( 'your_theme_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" >
 			<?php else : ?>
 				<img class="navbar-mobile-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/acrux-logo.png"> 
			<?php endif; ?>
</a>
<?php wp_nav_menu(array('theme_location' => 'main-menu', 'depth' => 3,)); ?>
<div class="header-social"> 
<div class="footer-menu social-menu">
      <ul>
      <?php if (get_option('facebook')):?><li><a href="<?php echo get_option('facebook'); ?>"><i class="fab fa-facebook"></i></a></li><?php endif ?>
      <?php if (get_option('twitter')):?><li><a href="<?php echo get_option('twitter'); ?>"><i class="fab fa-twitter"></i></a></li><?php endif ?>
      <?php if (get_option('instagram')):?><li><a href="<?php echo get_option('instagram'); ?>"><i class="fab fa-instagram"></i></a></li><?php endif ?>
      <?php if (get_option('youtube')):?><li><a href="<?php echo get_option('youtube'); ?>"><i class="fab fa-youtube"></i></a></li><?php endif ?>
      <?php if (get_option('vimeo')):?><li><a href="<?php echo get_option('vimeo'); ?>"><i class="fab fa-vimeo-v"></i></a></li><?php endif ?>
      <?php if (get_option('linkedin')):?><li><a href="<?php echo get_option('linkedin'); ?>"><i class="fab fa-linkedin"></i></a></li><?php endif ?>
    </ul>
</div>
</div>
</div>
<?php if( get_theme_mod( 'cd_navbar_position', 'bottom' ) == 'bottom' ) : ?>
<div class="navbar-mobile-close">Close</div>
</div>
<div class="navbar-mobile navbar-mobile-bottom">Menu</div>
<?php else: ?>	
</div>
<?php endif ?>	
</div>
</div>