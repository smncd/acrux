<?php 
/**
 * Desktop header content
 * 
 * @package acrux
 */
?>
<div class="navbar-desktop-container">
			<div class="navbar-desktop">
			<div class="navbar-desktop-logo-container">
			<a href="<?php echo home_url(); ?>">
			<?php if ( get_theme_mod( 'your_theme_logo' ) ) : ?>
 				<img class="navbar-desktop-logo" src="<?php echo get_theme_mod( 'your_theme_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" >
 			<?php else : ?>
 				<img class="navbar-desktop-logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/acrux-logo.png"> 
			<?php endif; ?>
				</a>
			</div>
			<div class="navbar-desktop-menu">								
			<?php wp_nav_menu(array('theme_location' => 'main-menu', 'depth' => 3,)); ?>
			</div>
			<div class="navbar-desktop-searchbar-container">
				<form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get" name="searchform" role="search">
                                <div class="searchbox">
                                    <input id="s" name="s" placeholder="Search..." type="text" value="">
                                    <button type="submit" id="searchsubmit" ><i class="fas fa-search"></i></button>
                                
                            </div>
                        </form>
			</div>	
			</div>
		</div>