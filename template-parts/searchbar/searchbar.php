<?php 
/**
 * Searchbar template part
 * 
 * @package acrux
 */
?>
<div class="searchbar-container">
				<form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get" name="searchform" role="search">
                                <div class="searchbox">
                                    <input id="s" name="s" placeholder="Search..." type="text" value="">
                                    <button type="submit" id="searchsubmit" ><i class="fas fa-search"></i></button>
                                
                            </div>
                        </form>
            </div>
