<?php 
/**
 * Main header
 * 
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package acrux
 */
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php the_title(); ?> | <?php $site_name = get_bloginfo( 'name' ); echo $site_name; ?></title>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/acrux-logo.png">
    <?php wp_head();?>
</head>
<body>
	
	<?php get_template_part('template-parts/header/header-desktop'); ?>	
	<?php get_template_part('template-parts/header/header-mobile'); ?>

                

	
  
