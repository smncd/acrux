<?php
/**
 * Acrux functions and definitions
 * *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package acrux
 */

/* Remove prefix for archive titles
---------------------------------------------------------------------------- */

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

/* ----------------------------------------------------------------------------  
 * Imports
 * ------------------------------------------------------------------------- */

/* Styles
-------------------------------------------- */
require get_template_directory() . '/inc/styles/styles.php';

/* Styles
-------------------------------------------- */
require get_template_directory() . '/inc/scripts/scripts.php';

/* Login
-------------------------------------------- */
require get_template_directory() . '/inc/login/login.php';

/* Register menus
---------------------------------------------------------------------------- */
require get_template_directory() . '/inc/menus/menus.php';

/* Load Customizer settings
-------------------------------------------- */
require get_template_directory() . '/inc/customizer/customizer.php';

/* Load settings page
-------------------------------------------- */
require get_template_directory() . '/inc/settings/settings.php';

/* Load WP-Admin settings
-------------------------------------------- */
require get_template_directory() . '/inc/wp-admin/wp-admin.php';

/* Load ACF Sync
-------------------------------------------- */
require get_template_directory() . '/inc/acf-json/acf-json.php';


