<?php 
/**
 * This page is called on 404 errors
 * 
 * @package acrux
 */
get_header(); ?>
	<main id="primary" class="site-main"> 
		<div class="entry-title">
			<h1>404</h1>
			<h3>This page does not exist.</h3>
		</div>
		<div class="page-content">
			<p>Try searching for it below:</p>
			<style>.searchbar-container{justify-content:unset; margin-bottom: 2rem;}</style>
			<?php get_template_part('template-parts/searchbar/searchbar'); ?>			
		</div>
	</main>
<?php get_footer(); ?>
