<?php 
/**
 * Searchbar template
 * 
 * @package acrux
 */
get_header();?>
<main id="primary" class="site-main"> 
<div class="page-content posts-page">
	<h1><?php printf(esc_html__('Search result for: %s'), '<span style="font-size: inherit">' . get_search_query() . '</span>'); ?></h1>
	
<div class="clear" style="height:50px;"></div>
	<?php if ( have_posts() ) : ?>
		
		<div class="row grid" style="padding-bottom: 50px;">
		
            <?php while ( have_posts() ) : the_post(); ?>
            		
				<div class="col col-12 col-sm-6 col-md-4 grid-item <?php foreach( (get_the_category() ) as $category) : ?> category-<?php echo $category->slug . ' '; endforeach; ?>" style="margin-bottom: 30px;">
					<div class="grid-item-inner">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
						<div class="grid-item-content">
							<a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
							
							
							<a class="button has-secondary-background-color" href="<?php echo get_the_permalink(); ?>">Read more</a>
						</div>
					</div>
				</div>
				
			<?php endwhile; ?>
			
		</div>
			
		<?php 
			the_posts_pagination( array(
			    'mid_size' => 2,
			    'prev_text' => __( '<i class="fal fa-long-arrow-left"></i>', 'textdomain' ),
			    'next_text' => __( '<i class="fal fa-long-arrow-right"></i>', 'textdomain' ),
			) ); 
		?>
            <?php else: ?>	
                Nothing found
	<?php endif; ?>
</div>
</main>
<?php get_footer(); ?>