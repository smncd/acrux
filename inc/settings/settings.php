<?php
/**
 * Add new menu for theme-options page with page callback theme-options-page.
 *
 * @package acrux
 */

function theme_option_page() {
?>
    <div class="wrap">
        <h1>Customize ACRUX</h1>
        <form method="post" action="options.php">
            <?php
            // display settings field on theme-option page
            settings_fields("theme-options-grp");
            // display all sections for theme-options page
            do_settings_sections("theme-options");
            submit_button();
            ?>
        </form>
    </div>
<?php
}
    
function add_theme_menu_item() {
    add_theme_page("ACRUX Customization", "ACRUX Customization", "manage_options", "theme-options", "theme_option_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");
    
function contact_details(){
    add_settings_section( 'contact_section', 'Contact Details', 'contact_section_description','theme-options');	
    add_settings_field('email_address', 'Email Address', 'display_email_element', 'theme-options', 'contact_section');
    register_setting( 'theme-options-grp', 'email');
    add_settings_field('country', 'Country', 'display_country_element', 'theme-options', 'contact_section');
    register_setting( 'theme-options-grp', 'country');	
}
add_action('admin_init','contact_details');

function contact_section_description(){
    echo '<p>Paste in the links (URLs) to your social media accounts.</p>';
}
function display_email_element(){
    echo '<input type="text" name="email" id="email" value="'. get_option('email') .'" />';
}
function display_country_element(){
    echo '<input type="text" name="country" id="country" value="'. get_option('country') .'" />';
}  
    
function social_links(){
    add_settings_section( 'social_section', 'Social Links', 'social_section_description','theme-options');	
    add_settings_field('facebook_url', 'Facebook Page Url', 'display_facebook_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'facebook');
    add_settings_field('twitter_url', 'Twitter Profile Url', 'display_twitter_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'twitter');
    add_settings_field('instagram_url', 'Instagram Profile Url', 'display_instagram_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'instagram');
    add_settings_field('youtube_url', 'YouTube Profile Url', 'display_youtube_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'youtube');
    add_settings_field('vimeo_url', 'Vimeo Profile Url', 'display_vimeo_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'vimeo');
    add_settings_field('linkedin_url', 'Linkedin Profile Url', 'display_linkedin_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'linkedin');
}
add_action('admin_init','social_links');

function social_section_description(){
    echo '<p>Paste in the links (URLs) to your social media accounts.</p>';
}
function display_twitter_element(){
    echo '<input type="text" name="twitter" id="twitter" value="'. get_option('twitter') .'" />';
}
function display_instagram_element(){
    echo '<input type="text" name="instagram" id="instagram" value="'. get_option('instagram') .'" />';
}
function display_facebook_element(){
    echo '<input type="text" name="facebook" id="facebook" value="'. get_option('facebook') .'" />';
}
function display_youtube_element(){
    echo '<input type="text" name="youtube" id="youtube" value="'. get_option('youtube') .'" />';
}
function display_vimeo_element(){
    echo '<input type="text" name="vimeo" id="vimeo" value="'. get_option('vimeo') .'" />';
}
function display_linkedin_element(){
    echo '<input type="text" name="linkedin" id="linkedin" value="'. get_option('linkedin') .'" />';
}    
    
function acrux_scripts(){
    add_settings_section( 'scripts_section', 'Scripts', 'scripts_section_description','theme-options');
    add_settings_field('fontawesome_url', 'Fontawesome Url', 'display_fontawesome_element', 'theme-options', 'scripts_section');
    register_setting( 'theme-options-grp', 'fontawesome_url');
}
add_action('admin_init','acrux_scripts');

function scripts_section_description(){
    echo '<p>Paste in the links (URLs) to the scripts here. This is very important for your site to display all content properly.</p>';}
function display_fontawesome_element(){ 
    echo '<input type="text" name="fontawesome_url" id="fontawesome_url" value="'. get_option('fontawesome_url') .'" />';
}