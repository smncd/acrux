<?php
/**
 * Enqueues theme scripts
 * 
 * @package acrux
 */

function acrux_enqueue_scripts() {

    wp_enqueue_script( 'acrux_fontawesome', get_option('fontawesome_url') );

    wp_enqueue_script( 'acrux_tether', 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js', array(), false, true );

    wp_enqueue_script( 'acrux_jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), false, true );

    wp_enqueue_script( 'acrux_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js', array(), false, true );

    wp_enqueue_script( 'acrux_elements', get_template_directory_uri() . '/assets/js/elements.js', array(), false, true );

    wp_enqueue_script( 'acrux_masonry', get_template_directory_uri() . '/assets/js/masonry.js', array(), false, true );
    
    wp_enqueue_script( 'acrux_navbar-desktop', get_template_directory_uri() . '/assets/js/navbar-desktop.js', array(), false, true );

    wp_enqueue_script( 'acrux_navbar-mobile', get_template_directory_uri() . '/assets/js/navbar-mobile.js', array(), false, true );

}
add_action( 'wp_enqueue_scripts', 'acrux_enqueue_scripts' );