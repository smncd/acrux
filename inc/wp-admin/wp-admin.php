<?php
/**
 * This file adds custom styling to the wp-admin dashbord
 *
 * @package acrux
 */

/* Admin Bar
---------------------------------------------------------------------------- */
function remove_admin_bar_links() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'wp-logo' );
	$wp_admin_bar->remove_menu( 'comments' );
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );


function main_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'main_menu' );


/* Custom Styling
---------------------------------------------------------------------------- */
function my_custom_fonts() {
  echo '
  <link rel="stylesheet" href="https://indestructibletype.com/fonts/Jost.css" type="text/css" charset="utf-8" />
  <style>

    :root {
      font-size: 18px;
    }

    body, td, textarea, input, select, .editor-styles-wrapper {
      font-family: "Jost", "Noto Serif" !important;
    } 

    .editor-styles-wrapper .rich-text.block-editor-rich-text__editable.wp-block-button__link{
      display: inline-block;
      margin: 0 auto;
      padding: 10px 25px;
      text-transform: uppercase;
      text-decoration: none;
      font-weight: 600;
      text-align: center;
      font-size: .86rem;
      border-radius: 20em;
      transition: all .2s ease;
      color: #ffffff;
      min-width: 8rem;
    }

   .editor-styles-wrapper h1 {
      font-size: 2.2rem;
      font-weight: 600;
      text-transform: uppercase;
      margin-top: 0;
    }
    
   .editor-styles-wrapper h2 {
      font-size: 1,9rem;
      font-weight: 600;
      text-transform: uppercase;
    }
    
   .editor-styles-wrapper h3 {
      font-size: 1,5rem;
      font-weight: 600;
      text-transform: uppercase;
    }
    
   .editor-styles-wrapper h4 {
      font-size: 1.3rem;
      font-weight: 600;
      text-transform: uppercase;
    }
  
   .editor-styles-wrapper h5 {
      font-size: 1.1rem;
      font-weight: 600;
      text-transform: uppercase;
    }
  
   .editor-styles-wrapper h6 {
      font-size: 1rem;
      font-weight: 600;
    }
    
    .editor-styles-wrapper a {
      text-decoration: none;
      color: var(--color-main);
      font-weight: bold;
    }
    
    .editor-styles-wrapper .main a {
      &:focus, &:hover {
        color: var(--color-main);
        text-decoration: underline;
      }
    }

    .editor-styles-wrapper p {
      font-size: 1rem !important;
      font-weight: 400;
    }
    
    .editor-styles-wrapper .wp-block-quote {
      border-left: 4px solid #000;
      padding-left: 1em;
    }
  </style>';
}
add_action('admin_head', 'my_custom_fonts');
