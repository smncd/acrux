<?php
/**
 * Defines WP editor colours and adds support for theme options
 * 
 * @package acrux
 */

// Add support for editor styles.
add_theme_support( 'editor-styles' );
// Add support for post thumbnails
add_theme_support( 'post-thumbnails' );
// Add support for wide-aligned images.
add_theme_support( 'align-wide' );

add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'Main', 'acrux' ),
		'slug'  => 'main',
		'color'	=> get_theme_mod('main_color', '#1a733f'),
	),
	array(
		'name'  => __( 'Secondary', 'acrux' ),
		'slug'  => 'secondary',
		'color' => get_theme_mod('second_color', '#25a44a'),
	),
	array(
		'name'  => __( 'Third', 'acrux' ),
		'slug'  => 'third',
		'color' => get_theme_mod('accent_color', '#8FD2EC'),
    ),

    array(
		'name'  => __( 'Smoke', 'acrux' ),
		'slug'  => 'smoke',
		'color' => '#eeeeee',
    ),

    array(
		'name'  => __( 'White', 'acrux' ),
		'slug'  => 'white',
		'color' => '#ffffff',
    ),
    array(
		'name'  => __( 'Black', 'acrux' ),
		'slug'  => 'black',
		'color' => '#000000',
    ),

    array(
		'name'  => __( 'Yellow', 'acrux' ),
		'slug'  => 'yellow',
		'color' => '#FDBF30',
    ),
    array(
		'name'  => __( 'Red', 'acrux' ),
		'slug'  => 'red',
		'color' => '#EF454E',
    ),
    array(
		'name'  => __( 'Orange', 'acrux' ),
		'slug'  => 'orange',
		'color' => '#FF7518',
    ),
) );