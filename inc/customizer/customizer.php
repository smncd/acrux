<?php 
/**
 * Adds functions to WP Customizer
 * 
 * @package acrux
 */

add_action( 'customize_register', 'cd_customizer_settings' );
function cd_customizer_settings( $wp_customize ) {
$wp_customize->add_panel( 'panel_acrux', array(
 'priority'       => 10,
  'capability'     => 'edit_theme_options',
  'title'          => __('ACRUX Customization'),
  'description'    => __('Customize theme'),
) );	
	
$wp_customize->add_section( 'section_colors' , array(
    'title'      => 'Colors',
    'priority'   => 20,
	    'panel'  => 'panel_acrux',
) );
	$wp_customize->add_setting( 'background_color' , array(
    'default'     => '#fff',
    'transport'   => 'refresh',
) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color', array(
	'label'        => 'Background Color',
	'section'    => 'section_colors',
	'settings'   => 'background_color',
) ) );
	$wp_customize->add_setting( 'main_color' , array(
    'default'     => '#1A733F',
    'transport'   => 'refresh',
) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_color', array(
	'label'        => 'Main',
	'section'    => 'section_colors',
	'settings'   => 'main_color',
) ) );	
	
	$wp_customize->add_setting( 'second_color' , array(
    'default'     => '#25a44a',
    'transport'   => 'refresh',
) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'second_color', array(
	'label'        => 'Second',
	'section'    => 'section_colors',
	'settings'   => 'second_color',
) ) );		
	
	$wp_customize->add_setting( 'accent_color' , array(
    'default'     => '#8FD2EC',
    'transport'   => 'refresh',
) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'accent_color', array(
	'label'        => 'Accent',
	'section'    => 'section_colors',
	'settings'   => 'accent_color',
) ) );		
	$wp_customize->add_setting( 'main_gray' , array(
    'default'     => '#333333',
    'transport'   => 'refresh',
) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_gray', array(
	'label'        => 'Main Gray',
	'section'    => 'section_colors',
	'settings'   => 'main_gray',
) ) );			
    $wp_customize->add_setting( 'navbar_color_inv' , array(
    'default'     => 0,
    'transport'   => 'refresh',
) );
    $wp_customize->add_control( 'header_color_inv', array(
    'label' => 'Invert header navigation bar color text.',
    'section' => 'section_colors',
    'settings' => 'navbar_color_inv',
    'type' => 'checkbox',
    'std' => 1,
) );
    $wp_customize->add_setting( 'nav_searchbar_color_inv' , array(
    'default'     => 0,
    'transport'   => 'refresh',
) );
    $wp_customize->add_control( 'nav_searchbar_color_inv', array(
    'label' => 'Invert header searchbar color text.',
    'section' => 'section_colors',
    'settings' => 'nav_searchbar_color_inv',
    'type' => 'checkbox',
    'std' => 1,
) );    



$wp_customize->add_section( 'section_navbar' , array(
    'title'      => 'Navigation Bar',
    'priority'   => 30,
	'panel'  => 'panel_acrux',
) );
		
	
$wp_customize->add_setting( 'cd_navbar_position' , array(
      'default'     => 'bottom',
      'transport'   => 'refresh',
) );
  $wp_customize->add_control( 'cd_navbar_position', array(
  'label' => 'Display the mobile navigation menu on the top or bottom of the screen.',
  'section' => 'section_navbar',
  'settings' => 'cd_navbar_position',
  'type' => 'radio',
  'choices' => array(
    'top' => 'Top',
    'bottom' => 'Bottom',
	  ),
) );

$wp_customize->add_section( 'section_footer' , array(
    'title'      => 'Footer',
    'priority'   => 40,
	'panel'  => 'panel_acrux',
) );
		
	
$wp_customize->add_setting( 'searchbar_footer' , array(
      'default'     => 1,
      'transport'   => 'refresh',
) );
  $wp_customize->add_control( 'searchbar_footer', array(
  'label' => 'Show searchbar in footer',
  'section' => 'section_footer',
  'settings' => 'searchbar_footer',
  'type' => 'checkbox',
  'std' => 1,
) );


$wp_customize->add_section( 'section_about' , array(
    'title'      => 'About',
    'priority'   => 50,
	'panel'  => 'panel_acrux',
) );
		
	
$wp_customize->add_setting( 'credit_footer' , array(
      'default'     => 1,
      'transport'   => 'refresh',
) );
  $wp_customize->add_control( 'credit_footer', array(
  'label' => 'Show appreciation by giving credit to the theme developer below the footer.',
  'section' => 'section_about',
  'settings' => 'credit_footer',
  'type' => 'checkbox',
  'std' => 1,
) );


// $wp_customize->add_section( 'section_fonts' , array(
//     'title'      => 'Font Size',
//     'priority'   => 60,
// 	'panel'  => 'panel_acrux',
// ) );
// $wp_customize->add_setting( 'toggle_fonts' , array(
//     'default'     => 1,
//     'transport'   => 'refresh',
// ) );
// $wp_customize->add_control( 'toggle_fonts', array(
// 'label' => 'Enable custom font sizes',
// 'section' => 'section_fonts',
// 'settings' => 'toggle_fonts',
// 'type' => 'checkbox',
// 'std' => 1,
// ) );

// $wp_customize->add_setting( 'banner_h1_size' , array(
//     'default'     => 'Come On In',
//     'transport'   => 'postMessage',
// ) );

// $wp_customize->add_control( 'banner_h1_size', array(
//     'label' => 'Homepage banner H1 size',
// 'section'	=> 'section_fonts',
// 'type'	 => 'text',
// ) );


$wp_customize->add_section( 'section_front_page' , array(
    'title'      => 'Front Page',
    'priority'   => 20,
	    'panel'  => 'panel_acrux',
) );

$wp_customize->add_setting( 'front_page_hide_title' , array(
    'default'     => 0,
    'transport'   => 'refresh',
) );
$wp_customize->add_control( 'front_page_hide_title', array(
'label' => 'Hide title and static banner image on front page. Useful for plugins like Gutenslider.',
'section' => 'section_front_page',
'settings' => 'front_page_hide_title',
'type' => 'checkbox',
'std' => 1,
) );

$wp_customize->add_setting( 'front_page_gutenslider_overlay' , array(
    'default'     => 1,
    'transport'   => 'refresh',
) );
$wp_customize->add_control( 'front_page_gutenslider_overlay', array(
'label' => 'Enable overlay for GutenSlider carousel',
'section' => 'section_front_page',
'settings' => 'front_page_gutenslider_overlay',
'type' => 'checkbox',
'std' => 1,
) );

$wp_customize->add_setting( 'posts_front_page' , array(
    'default'     => 0,
    'transport'   => 'refresh',
) );
$wp_customize->add_control( 'posts_front_page', array(
'label' => 'Show most recent posts on static home page.',
'section' => 'section_front_page',
'settings' => 'posts_front_page',
'type' => 'checkbox',
'std' => 1,
) );

$wp_customize->add_setting( 'posts_front_page_number' , array(
    'default'     => 3,
    'transport'   => 'refresh',
) );
$wp_customize->add_control( 'posts_front_page_number', array(
'label' => 'Choose how many posts to display',
'section' => 'section_front_page',
'settings' => 'posts_front_page_number',
'type' => 'number',
'std' => 1,
) );
}



add_action( 'wp_head', 'cd_customizer_css');
function cd_customizer_css()
{
    ?>
         <style type="text/css">
			 :root{
				 	--color-main: <?php echo get_theme_mod('main_color', '#1a733f');?>;
					--color-second: <?php echo get_theme_mod('second_color', '#25a44a');?>;
					--color-third: <?php echo get_theme_mod('accent_color', '#8FD2EC');?>;
					--color-dark: <?php echo get_theme_mod('main_gray', '#333333');?>;
					/*--font-gray: <?php echo get_theme_mod('main_color', '#464646');?>;*/
					}
             body { background: #<?php echo get_theme_mod('background_color', '#fff'); ?>; }

                <?php if ( get_theme_mod( 'navbar_color_inv' ) ) : ?>
            .menu-main-container a{color:#000 !important;}
            .navbar-desktop-menu li{color:#00000052 !important;}
                <?php endif ?>
                <?php if ( get_theme_mod( 'nav_searchbar_color_inv' ) ) : ?>
            .searchbox{color:#000 !important;}
                <?php endif ?>

         </style>
    <?php
}


if (get_theme_mod('front_page_gutenslider_overlay')) {
    function my_custom_styles()
    {
     echo "
     <style>
     .wp-block-eedee-block-gutenslide .slide-content {
        background-color: #0f0f0f2e;
     }
    </style>
    ";
    }
    add_action('wp_head', 'my_custom_styles');
    }

/**
* Custom Logo
*/
function your_theme_new_customizer_settings($wp_customize) {
// add a setting for the site logo
$wp_customize->add_setting('your_theme_logo');
// Add a control to upload the logo
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'your_theme_logo',
array(
'label' => 'Upload Logo',
'section' => 'title_tagline',
'settings' => 'your_theme_logo',
) ) );
}
add_action('customize_register', 'your_theme_new_customizer_settings');