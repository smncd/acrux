<?php
/**
 * Enqueues theme styling
 * 
 * @package acrux
 */

function acrux_enqueue_styles() {

    wp_enqueue_style( 'acrux_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css' );

    wp_enqueue_style( 'acrux_jost', 'https://indestructibletype.com/fonts/Jost.css' );

    wp_enqueue_style( 'acrux_global', get_template_directory_uri() . '/assets/css/global.css' );
    
    if (get_theme_mod('posts_front_page')):
        wp_enqueue_style( 'acrux_latest-posts', get_template_directory_uri() . '/assets/css/latest-posts.css' );
    endif;
}
add_action( 'wp_enqueue_scripts', 'acrux_enqueue_styles' );