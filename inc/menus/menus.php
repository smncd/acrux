<?php
/**
 * Register theme menus
 * 
 * @package acrux
 */

function acrux_theme_menus() {
    register_nav_menus(
      array(
        'footer-menu' => __( 'Footer Menu' ),
        'social-menu' => __( 'Social Menu' )
      )
    );
}
add_action( 'init', 'acrux_theme_menus' );