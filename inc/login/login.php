<?php
/**
 * Customisation for the login page
 * 
 * @package acrux
 */

function change_login_logo_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'change_login_logo_url' );

function change_login_logo_url_title() {
	return "acrux";
}
add_filter( 'login_headertext', 'change_login_logo_url_title' );

function change_login_stylesheet() {
	wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/assets/css/custom-login.css' );
}
add_action( 'login_enqueue_scripts', 'change_login_stylesheet' );