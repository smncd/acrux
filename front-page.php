<?php
/**
 * The Front Page
 * 
 * @package acrux
 */
get_header(); ?>
<main id="primary" class="site-main page-keyvisual">  
<?php 
$site_description = get_bloginfo( 'description', 'display' );
$site_name = get_bloginfo( 'name' );
?>
<?php 
if( get_theme_mod('front_page_hide_title')):
?>
<div class="page-content page-keyvisual">
<?php
else:   
if (get_field('banner_image')): ?>
<div class="banner">
    <div class="banner-img banner-img-full banner-overlay d-flex" style="background:url('<?php the_field('banner_image'); ?>');">
    	<div class="page-title d-flex">
    		<h1><?php echo $site_name; ?></h1>
    		<h2><?php echo $site_description; ?></h2>
    		<a class="button btn-color-third" href="<?php the_field('banner_cta_link'); ?>"><?php the_field('banner_cta_title'); ?></a>

    	</div>
    </div>
</div>
<div class="page-content">
<?php else: ?>
<div class="page-content">
    <div class="entry-title">
    <h1><?php echo $site_name; ?></h1>
    </div>
<?php 
endif;
endif;
 ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
    <?php the_content(); ?>
    </div>
    <?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>
</div>
<?php if (get_theme_mod('posts_front_page')): get_template_part( 'template-parts/content/entry', 'latest-posts' ); endif ?>
</main>
<?php get_footer(); ?>