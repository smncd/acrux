rm -rf acrux.zip 
echo "removed old archive"
if [ "$1" == "dev" ]; then 
    node-sass assets/css/src -o assets/css/ && 
    sed -i "s/Version: /Version: beta/g" style.css && 
    sed -i "s/Theme Name: ACRUX/Theme Name: ACRUX (BETA)/g" style.css && 
    zip -r acrux.zip ./ -x ".git/*" ".gitignore" "package.json" "package-lock.json" "README.md" "assets/css/src/*" "node_modules/*" && 
    sed -i "s/Version: beta/Version: /g" style.css &&
    sed -i "s/Theme Name: ACRUX (BETA)/Theme Name: ACRUX/g" style.css 
elif [ "$1" == "production" ]; then  
    node-sass assets/css/src -o assets/css/ --output-style compressed && 
    zip -r acrux.zip ./ -x ".git/*" ".gitignore" "package.json" "package-lock.json" "README.md" "assets/css/src/*" "node_modules/*" 
fi
echo "  $1 bundle complete"
echo "
          _  _         
     /\  /  |_) | | \/ 
    /--\ \_ | \ |_| /\ 
                    
"